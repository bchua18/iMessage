package com.example.chua.imessages.items

import android.content.Context
import com.example.chua.imessages.R
import com.example.chua.imessages.models.TextMessage
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.message_row.*

class MessageInboxItem(val message: TextMessage, val context: Context) : MessageItem(message) {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.tvUserName.text = message.senderName
        viewHolder.tvUserMessage.text = message.text
        viewHolder.tvTime.text = message.time.toString()
        super.bind(viewHolder, position)
    }

    override fun getLayout(): Int = R.layout.message_row
}