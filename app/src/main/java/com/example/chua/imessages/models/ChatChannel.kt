package com.example.chua.imessages.models

data class ChatChannel(val userIds: MutableList<String>) { constructor() : this(mutableListOf()) }