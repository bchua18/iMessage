package com.example.chua.imessages.items

import android.content.Context
import com.example.chua.imessages.R
import com.example.chua.imessages.models.User
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.contact_row.*

class PersonItem(val person: User, val userId: String, private val context: Context) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.tvUserName.text = person.name
        viewHolder.tvUserEmail.text = person.email
    }

    override fun getLayout() = R.layout.contact_row
}