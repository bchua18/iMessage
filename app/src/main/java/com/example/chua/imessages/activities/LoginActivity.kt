package com.example.chua.imessages.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Patterns
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.chua.imessages.MainActivity
import com.example.chua.imessages.R
import com.example.chua.imessages.receivers.NetworkChangeReceiver
import com.example.chua.imessages.utils.FirestoreUtil
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.AuthUI.IdpConfig
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.longSnackbar

class LoginActivity : AppCompatActivity(), View.OnFocusChangeListener {

    private var mProviders: ArrayList<IdpConfig>? = null

    companion object {
        var mAuth: FirebaseAuth? = null
        const val RC_SIGN_IN = 100

        fun createIntent(context: Context): Intent =
                Intent(context, LoginActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

        fun isValidEmail(target: CharSequence): Boolean =
                !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()

        fun hideKeyboard(view: View?) =
                (view?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        .hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()
        mProviders = ArrayList()

        etEmail.onFocusChangeListener = this
        etPassword.onFocusChangeListener = this
        btnSignIn.isEnabled = false

        btnSignIn.setOnClickListener { login() }

        btnSignUp.setOnClickListener {
            etEmail.setText(""); etPassword.setText("")
            register()
        }

        ivFacebook.setOnClickListener {
            mProviders?.add(IdpConfig.FacebookBuilder().build())
            activityResult()
        }

        ivTwitter.setOnClickListener {
            mProviders?.add(IdpConfig.TwitterBuilder().build())
            activityResult()
        }

        ivGoogle.setOnClickListener {
            mProviders?.add(IdpConfig.GoogleBuilder().build())
            activityResult()
        }
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        val email = etEmail.text.toString().trim()
        val password = etPassword.text.toString().trim()
        val check = ContextCompat.getDrawable(this, R.drawable.ic_check_circle)

        check?.setBounds(0, 0, check.intrinsicWidth, check.intrinsicHeight)

        when (v?.id) {
            R.id.etEmail -> {
                if (TextUtils.isEmpty(email))
                    etEmail.error = null
                else if (!hasFocus && isValidEmail(email))
                    etEmail.setError("Email is valid", check)
                else if (!(hasFocus || isValidEmail(email)))
                    etEmail.error = getString(R.string.text_invalid_email)
            }
            R.id.etPassword -> {
                if (TextUtils.isEmpty(password))
                    etPassword.error = null
                else if (!hasFocus && password.length >= 8)
                    etPassword.setError("Password is valid", check)
                else if (!(hasFocus || password.length >= 8))
                    etPassword.error = getString(R.string.text_invalid_password)
            }
        }

        if (isValidEmail(email) && password.length >= 8) {
            btnSignIn.background = ContextCompat.getDrawable(this, R.drawable.button_color)
            btnSignIn.isEnabled = true
        }
        else {
            btnSignIn.background = ContextCompat.getDrawable(this, android.R.drawable.btn_default)
            btnSignIn.isEnabled = false
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev?.action == MotionEvent.ACTION_DOWN) {
            val view = currentFocus
            if (etEmail.isFocused || etPassword.isFocused) {
                val outRect = Rect()
                etEmail.getGlobalVisibleRect(outRect)
                etPassword.getGlobalVisibleRect(outRect)
                if (!outRect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                    etEmail.clearFocus(); etPassword.clearFocus()
                    hideKeyboard(view)
                }
            }
        }

        return super.dispatchTouchEvent(ev)
    }

    private fun login() {
        val email = etEmail.text.toString().trim()
        val password = etPassword.text.toString().trim()
        val progressDialog = indeterminateProgressDialog("Logging in...")

        mAuth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener(this) {
            if (it.isSuccessful)
                startMainActivity(null)
            else {
                etPassword.setText(""); etPassword.error = null
                longSnackbar(findViewById(android.R.id.content), "Invalid username and/or password.")
            }

            progressDialog.dismiss()
        }
    }

    private fun register() {
        val register = Intent(this, RegisterActivity::class.java)
        startActivity(register)
    }

    private fun activityResult() {
        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(mProviders!!).build(), RC_SIGN_IN)
        mProviders?.clear()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK)
                startMainActivity(response)
            else {
                if (response == null) return

                when (response.error?.errorCode) {
                    ErrorCodes.NO_NETWORK -> longSnackbar(findViewById(android.R.id.content), "No network.")
                    ErrorCodes.UNKNOWN_ERROR -> longSnackbar(findViewById(android.R.id.content), "Unknown error.")
                }
            }
        }
    }

    private fun startMainActivity(response: IdpResponse?) {
        val progressDialog = indeterminateProgressDialog("Setting up your account")

        FirestoreUtil.addCurrentUser {
            startActivity(MainActivity.createIntent(this, response))
            progressDialog.dismiss()
        }
    }
}