package com.example.chua.imessages.utils

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtil {
    private const val TYPE_CONNECTED = 1
    private const val TYPE_NOT_CONNECTED = 0

    private fun getConnectivityStatus(context: Context?): Int {
        val conMgr = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = conMgr.activeNetworkInfo

        if (netInfo != null || netInfo?.type == ConnectivityManager.TYPE_WIFI ||
                netInfo?.type == ConnectivityManager.TYPE_MOBILE)
            return TYPE_CONNECTED

        return TYPE_NOT_CONNECTED
    }

    fun getConnectivityStatusString(context: Context?): String {
        val conn = NetworkUtil.getConnectivityStatus(context)
        val status: String

        status = when (conn) {
            NetworkUtil.TYPE_CONNECTED -> "Connected to the internet"
            else -> "No internet connection"
        }

        return status
    }
}