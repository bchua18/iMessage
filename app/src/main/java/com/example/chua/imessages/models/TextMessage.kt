package com.example.chua.imessages.models

import com.example.chua.imessages.activities.ChatActivity
import java.util.*

class TextMessage(val text: String, override val time: Date, override val senderId: String,
                  override val recipientId: String, override val senderName: String,
                  override val type: String = ChatActivity.KEY_TEXT) : Message {
    constructor() : this("", Date(0), "", "", "")
}