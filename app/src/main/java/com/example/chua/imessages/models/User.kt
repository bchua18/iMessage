package com.example.chua.imessages.models

data class User(val name: String, val email: String, val contactNumber: String,
                 val imagePath: String?, val registrationTokens: MutableList<String>) {
    constructor(): this("", "", "", null, mutableListOf())
}