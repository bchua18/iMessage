package com.example.chua.imessages.receivers

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.chua.imessages.utils.NetworkUtil
import org.jetbrains.anko.design.longSnackbar

class NetworkChangeReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val status = NetworkUtil.getConnectivityStatusString(context)

        longSnackbar((context as Activity).findViewById(android.R.id.content), status)
    }
}