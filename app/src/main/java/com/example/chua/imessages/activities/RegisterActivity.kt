package com.example.chua.imessages.activities

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.example.chua.imessages.R
import com.example.chua.imessages.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.indeterminateProgressDialog

class RegisterActivity : AppCompatActivity(), TextWatcher, View.OnClickListener, View.OnFocusChangeListener {

    private var mAuth: FirebaseAuth? = null
    private var mDb: FirebaseFirestore? = null
    private var mUser: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mAuth = FirebaseAuth.getInstance()
        mDb = FirebaseFirestore.getInstance()

        setViews()
        setListeners()
    }

    private fun setViews() {
        btnCreateAccount.isEnabled = false
        tvShowHide1.visibility = View.GONE; tvShowHide2.visibility = View.GONE
        etPassword.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT
        etConfirmPassword.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT
    }

    private fun setListeners() {
        etPassword.addTextChangedListener(this); etConfirmPassword.addTextChangedListener(this)
        tvShowHide1.setOnClickListener(this); tvShowHide2.setOnClickListener(this)
        btnCreateAccount.setOnClickListener(this); tvSignIn.setOnClickListener(this)
        etUsername.onFocusChangeListener = this; etEmailAddress.onFocusChangeListener = this
        etPassword.onFocusChangeListener = this; etConfirmPassword.onFocusChangeListener = this
    }

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        displayShowHide(etPassword, tvShowHide1)
        displayShowHide(etConfirmPassword, tvShowHide2)
    }

    private fun displayShowHide(editText: EditText?, textView: TextView?) {
        if (editText?.text!!.isNotEmpty()) textView?.visibility = View.VISIBLE
        else textView?.visibility = View.GONE
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        val userName = etUsername.text.toString().trim()
        val email = etEmailAddress.text.toString().trim()
        val phoneNumber = etPhoneNumber.text.toString().trim()
        val password = etPassword.text.toString().trim()
        val confirmPassword = etConfirmPassword.text.toString().trim()

        when (v?.id) {
            R.id.etUsername -> {
                if (!hasFocus && TextUtils.isEmpty(userName))
                    etUsername.error = getString(R.string.text_required)
            }
            R.id.etEmailAddress -> {
                if (TextUtils.isEmpty(email))
                    etEmailAddress.error = null
                else if (!(hasFocus || LoginActivity.isValidEmail(email)))
                    etEmailAddress.error = getString(R.string.text_invalid_email)
            }
            R.id.etPassword -> {
                if (TextUtils.isEmpty(password))
                    etPassword.error = null
                else if (!(hasFocus || password.length > 8))
                    etPassword.error = getString(R.string.text_invalid_password)
            }
            R.id.etConfirmPassword -> {
                if (TextUtils.isEmpty(confirmPassword))
                    etConfirmPassword.error = null
                else if (!(hasFocus || password == confirmPassword))
                    etConfirmPassword.error = getString(R.string.text_password_unmatch)
            }
        }

        if (!TextUtils.isEmpty(userName) && LoginActivity.isValidEmail(email) && password == confirmPassword) {
            mUser = User(userName, email, phoneNumber, null, mutableListOf())
            btnCreateAccount.background = ContextCompat.getDrawable(this, R.drawable.button_color)
            btnCreateAccount.isEnabled = true
        }
        else {
            btnCreateAccount.background = ContextCompat.getDrawable(this, android.R.drawable.btn_default)
            btnCreateAccount.isEnabled = false
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvShowHide1 -> toggleShowHide(etPassword, tvShowHide1)
            R.id.tvShowHide2 -> toggleShowHide(etConfirmPassword, tvShowHide2)
            R.id.btnCreateAccount -> {
            val progressDialog = indeterminateProgressDialog("Registering account...")

            mAuth?.createUserWithEmailAndPassword(mUser!!.email, etPassword.text.toString().trim())
                    ?.addOnCompleteListener {
                if (it.isSuccessful) {
                    mDb?.collection("users")?.document(mAuth?.currentUser!!.uid)?.set(mUser!!)
                    longSnackbar(findViewById(android.R.id.content), "Account created successfully")
                    etUsername.setText(""); etEmailAddress.setText(""); etPhoneNumber.setText("")
                    etPassword.setText(""); etConfirmPassword.setText("")
                }
                else
                    longSnackbar(findViewById(android.R.id.content), "Registration has encountered an error")

                progressDialog.dismiss()
            }
        }
            else -> finish()
        }
    }

    private fun toggleShowHide(editText: EditText?, textView: TextView?) {
        if (textView?.text == getString(R.string.text_show)) {
            textView?.text = getString(R.string.text_hide)
            editText?.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            editText?.setSelection(etPassword.length())
        }
        else {
            textView?.text = getString(R.string.text_show)
            editText?.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT
            editText?.setSelection(etPassword.length())
        }
    }
}