package com.example.chua.imessages

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.view.Menu
import android.view.MenuItem
import com.example.chua.imessages.activities.LoginActivity
import com.example.chua.imessages.fragments.*
import com.example.chua.imessages.receivers.NetworkChangeReceiver
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.firebase.ui.auth.util.ExtraConstants
import com.google.firebase.auth.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class MainActivity : AppCompatActivity() {

    private var mToolbar: ActionBar? = null

    companion object {
        const val KEY_USER_ID = "id"
        const val KEY_USER_NAME = "name"

        fun createIntent(context: Context, idpResponse: IdpResponse?): Intent =
                Intent().setClass(context, MainActivity::class.java)
                        .putExtra(ExtraConstants.IDP_RESPONSE, idpResponse)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.registerReceiver(NetworkChangeReceiver(), IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
        mToolbar = supportActionBar
        mToolbar?.title = getString(R.string.text_action_item_1)

        replaceFragment(ContactsFragment.newInstance())

        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_item_1 -> {
                    replaceFragment(ContactsFragment.newInstance())
                    mToolbar?.title = getString(R.string.text_action_item_1)
                }
                R.id.action_item_2 -> {
                    replaceFragment(MessagesFragment.newInstance())
                    mToolbar?.title = getString(R.string.text_action_item_2)
                }
                R.id.action_item_3 -> {
                    replaceFragment(AccountFragment.newInstance())
                    mToolbar?.title = getString(R.string.text_action_item_3)
                }
                R.id.action_item_4 -> {
                    replaceFragment(SettingsFragment.newInstance())
                    mToolbar?.title = getString(R.string.text_action_item_4)
                }
            }

            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun onStart() {
        super.onStart()

        if (FirebaseAuth.getInstance().currentUser == null) {
            alert("Session expired. Please sign in again.") {
                yesButton {
                    startActivity(LoginActivity.createIntent(this@MainActivity))
                    finish()
                }
            }.show()
        }
    }

    private fun replaceFragment(fragment: Fragment?) {
        supportFragmentManager.beginTransaction().replace(R.id.fragment, fragment).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_search, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_logout -> {
                AuthUI.getInstance().signOut(this).addOnSuccessListener {
                    startActivity(LoginActivity.createIntent(this@MainActivity))
                    finish()
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }
}