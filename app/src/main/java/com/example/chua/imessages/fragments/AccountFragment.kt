package com.example.chua.imessages.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.chua.imessages.R
import com.example.chua.imessages.utils.FirestoreUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : Fragment() {

    companion object {
        fun newInstance() : AccountFragment = AccountFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onStart() {
        super.onStart()
        FirestoreUtil.getCurrentUser { user ->
            if (this@AccountFragment.isVisible) {
                tvName.text = user.name
                tvContactNumber.text = user.contactNumber
                tvEmailAddress.text = user.email

                if (user.imagePath != null)
                    Picasso.with(context).load(user.imagePath).into(ivImage)
            }
        }
    }
}
